# dwh-soft

#### 介绍
用来存放dwh服务同步组件的程序包 <br>
这是一款基于 mysql 数据库迁移或实时同步到 postgresql 的中间服务组件(可断网执行，安全隔离，无需外网接入，免费绿色软件)  <br>
该程序主要用于解决 mysql/mariadb 同步到 PG\Mysql\greenplum\cloudBerryDb 等场景的用途，目的是把PG当成读库来操作，全程自动化，类似于读写分离的效果<br>

![软件效果图](rjjs.png)

#### 软件架构
使用java开发的二进制程序包，基于web应用程序架构。<br>
无需连接外网，可断网模式运行，本机启动即可运行的web应用程序包<br>
类似于阿里云的 dts 数据同步服务(腾讯、字节跳动都有类似服务)，也可以用于 mysql 数据不停机服务线下、线上的迁移和实时同步，或海外机房同步等，可节约大量资金<br>

架构补充<br>
前端框架采用的是MIT协议的 [pearAdmin UI](https://gitee.com/pear-admin/Pear-Admin-Layui) <br>
后端框架采用 [quarkus](https://gitee.com/mirrors/Quarkus) 框架 + 数据操作组件 [edb](https://github.com/MrYang-Jia/edb)<br>


#### 安装教程
初始化admin账号密码(可修改): dwh/123456 <br>

windows
```properties
# 下载 dwh-1.2.0-windows.zip 解压后，放到您指定的目录下启动即可
# 如果需要修改默认端口号，则需要通过 cmd 或 linux控制台指定端口号(Dquarkus.http.port) , 否则直接启动即可
# 默认端口访问地址： http://127.0.0.1:11306/dwh/login
./dwh-1.2.0.exe -Dquarkus.http.port=11306

```

linux centos7(推荐)
```properties
# 建议使用 centos7 或以上版本
# 下载 dwh-1.2.0-linux.zip 解压后，放到您指定的目录下启动即可
# 设置文件为可启动文件
chmod 777 ./dwh-1.2.0
# 使用screen后台模式
yum -y install screen
# 启动一个窗口
screen -S dwh
# 启动服务（无需后台挂起 ctrl+z ,然后后台模式 bg）
# 默认端口访问地址： http://127.0.0.1:11306/dwh/login
./dwh-1.2.0 -Dquarkus.http.port=11306
# screen会话中：快捷键 ctrl+a+d ，退出会话，将会话切换到后台运行,这样子退出sshd控制台时，服务能正常运行，不影响服务的运行

```


#### 使用说明
软件使用期限每半年更新一次（尽量让大家不会太麻烦），因为我不想被人直接挂售。 <br>
<br>首次使用时，输入初始化账号密码: dwh/123456 ,
<br>然后打开【数据同步】-【任务管理】，点击【创建任务】创建迁移同步任务即可
<br>第一次使用时，直接迁移测试用的小数据库，然后点击同步即可。
<br>当然，如果您熟悉了这套软件，也可以直接用于生产迁移数据，但是建议是从有读写分离的负载库上迁移，减少数据库的影响
<br>同时也支持，您自己手工指定对应的 binlog 日志节点，进行指定数据同步
<br>
[dwh mysql同步软件介绍](https://mp.weixin.qq.com/s?__biz=MjM5MTY2MjcxMQ==&mid=2247483659&idx=1&sn=d0a1b3a57d698b8f901646fad0bda89d&chksm=a6b3561391c4df05b74a71ffce1fbdd15f0dcb6dca9995e407c10d5e65667bf805051e539f79)
<br>
[软件使用讲解](https://www.bilibili.com/video/BV1zj421X7ni/)
<br>
[dwh同步软件使用教程-图文](https://mp.weixin.qq.com/s?__biz=MjM5MTY2MjcxMQ==&mid=2247483684&idx=1&sn=ea48be3562ac9369ce7abacd6de2263d&chksm=a6b3563c91c4df2a31f63c823bd4f40ad5ef3e627b6446230756843aa7983f8e518265d8de6d)
#### 参与贡献
因为刚开始，所以短期内可能不会有太大变动的内容，有紧急诉求的伙伴，可以通过公众号单独在后台留下联系方式，我看到后会及时联系
[公众号](https://mp.weixin.qq.com/s?__biz=MjM5MTY2MjcxMQ==&mid=2247483659&idx=1&sn=d0a1b3a57d698b8f901646fad0bda89d&chksm=a6b3561391c4df05b74a71ffce1fbdd15f0dcb6dca9995e407c10d5e65667bf805051e539f79)

#### 特技
mysql 无人值守全量全库自动同步到Postgresql数据库，实现异构数据库读库功能<br>
mysql 实现数据迁移、同步到 mysql、mariadb、postgresql、greenplum、cloudBerryDb、kafka等数据存储点<br>
一键增加Mysql常用兼容函数 if、ifnull、group_concat、find_in_set、sysdate()、date_format<br>
ps: kafka 模块没有经过严格验证，后续我再完善下，其他模块都是经过了百分百的时间论证过的结果，总耗时5年左右完成的稳定异构数据库同步，目前这个版本是第三代版本，所以实际上还有很多同步模块的功能并没有实现，但是这个工具的价值就在于异构数据库的同步，有其他诉求的可以留言，有场景化了，才继续延申扩展<br>


#### mysql 数据类型支持
![img.png](img.png)

#### greenplum / cloudBerryDb 特殊说明
由于 cloudBerryDb 是基于 greenplum 的一些基础版本规则构建的，所以两者可以认为是等同的分布式数据库，其中 cloudBerryDb 底层是PG14版本，优于GP6，测试性能比GP6高7倍以上<br>
目前同步时，默认以 mysql 的主键表位主键进行分布式hash键值分布，如果同步时也同步索引，则自动构建基于主键的分布式索引<br>
如果修改主键时，会自动构建新的分布式键，但是原索引会全部移除，因为所有的索引是依照主键去构建的，然后再重新迁移节点数据<br>
如果是无主键的表，则会使用hash随机分布节点数据<br>
特别说明，如果需要使用gis数据类型的，需要额外安装相关 postgis 库的插件，尤其是分布式数据库 cloudBerryDb 对应的库为 [cloudberrydb_gis](https://github.com/cloudberrydb/postgis)

#### 若依系统怎么迁移到PG/greenplum/cloudBerryDb上？
具体信息可以参考当前目录底下的 【若依加速PG版文件/README.md】

#### 部分软件截图
> ##### 登录页
![dly.png](dly.png)
> ##### 同步功能页
![tby.png](tby.png)
2024-10-19 增加表单类型的配置页，方便用户快速配置同步任务<br>
> ##### 导入功能页
![dry.png](dry.png)
![drjg.png](drjg.png)
> ##### 导入管理
![dry.png](drgl-01.png)
可以批量处理导入异常的表，也可以对齐突然中断，未完全对齐的导入表信息，再重新导入<br>
![dry.png](drgl-02.png)
> ##### 同步功能属性项
2024.10.19 额外增加对 greenplum/cloudberryDb的同步支持<br>
2024.10.19 额外增加 同步表覆盖策略，同时主同步策略同步时增加只同步同步表覆盖策略的同步功能，区别于 ETL 同步，因为ETL同步不支持ddl表结构变更调整同步<br>
2024.10.19 修复主键id变更时，数据无法同步的问题<br>
![tbgn.png](tbgn.png)
> ##### 系统告警通知功能
##### 支持 钉钉BOT、微信BOT、Http接口调用、阿里云短信、邮件通知等方式
![tzgn.png](tzgn.png)
> ##### 异常定位
![ycdw.png](ycdw.png)
> ##### 断网自动重连
![ycdw.png](cqrz.png)
2024.10.19 断网自动重连功能，发现如果是io异常时，则会自动重连，间隔3分钟，如果有设置告警提醒，则会再每间隔5次则会发送一次告警信息进行推送<br>
#### 可关注公众号私信
![gzh.png](gzh.png)

#### 补充说明，因为每次上传新的包，这个空间会越来越大，所以会不断删库，重新创建库然后上传版本包，避免大家下载的时候项目太大
2024-11-25 <br>
  1、修正binlog包因为网络问题，可能导致的数据丢失问题，直接断开，重新同步即可<br>
  2、修正源表结构无法自定义添加的问题，因为某些情况下，同步断开很久，导致线上的表结构信息无法对上，需要手工维护映射，以便当前节点能正常同步<br>
2024-11-30 <br>
  1、修复创建任务时，任务编码为空未进行校验的bug <br>
  2、调整断线重连为10次一次提醒 <br>
2024-12-05 <br>
1、修正可运行列表的任务里表结构缓存无法刷新的bug <br>
2025-01-22 <br>
1、增加导入管理页面的纠错比对导入表的功能，并允许异常表一键重导功能 <br>
2、修复同步数据时，如果是mysql库同步，出现主键键值修改的情况下，导致数据更新报错的问题。（postgresql模式下无影响，主要是 ` 字符和 " 字符操作影响） <br>



