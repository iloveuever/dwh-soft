
### 若依版本地址(v4.7.8)：https://gitee.com/y_project/RuoYi
目前是将修改后的代码直接打包压缩上传，便于大家对照修改的信息项
### 视频地址: https://www.bilibili.com/video/BV1E2421M7yv/

### 方案2：迁移到PG/GreenPlum/cloudBerryDb作为主数据库（有同学第一次使用若依，但是想直接上手PG怎么办，可以尝试这个方案）

#### 第1步 配置新的PG数据源信息

```
 修改若依数据库的连接信息为PG数据源信息，但是连接参数不要增加 &stringtype=unspecified ，该参数会影响quartz的集群执行
```

```yaml
#### PageHelper分页插件改成PG模式
pagehelper:
    helperDialect: postgresql
    reasonable: true
    supportMethodsArguments: true
    params: count=countSql
```




#### 第2步 改部分代码和sql脚本
```
# 替换系统的代码
# 寻找 replace into  table (全文基本上就一处代码)
# 替换成pg的插入失败即更新的模式
    insert into table values()
    on conflict(主键字段) DO
    update set f1=xxx,f2=xxx ;
```


```
#### 全局替换 (只需要操作两次即可) (避免PG报类型不一致的异常信息)
Mapper.xml中
status = 0 为 status = '0'
visible = 0 为 visible = '0'

# 替换 quartz 表结构为 boolean 类型，不然 varchar(1) 类型在pg解析器底下无法正常转换成 boolean 类型 (如果是老系统，已经是错误的数据类型，则需要手工调整数据库的表字段类型)
quartz.sql 中，如果是数据库，则需要单独一个个表去修改
varchar(1) 替换为 boolean

```


```
# 修改定时任务配置
/**
 * 定时任务配置（单机部署建议默认走内存，如需集群需要创建qrtz数据库表/打开类注释）
 *
 * @author ruoyi
 */
@Configuration
public class ScheduleConfig
{
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource)
    {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setDataSource(dataSource);
        
        ......
        
        // 新增pg库时启用该参数
        prop.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.PostgreSQLDelegate");
        
        ......

        return factory;
    }
}

```





#### 第3步 dwh迁移数据
```
# 先将mysql的表结构导入到mysql库
# 操作dwh软件迁移数据
# 迁移数据 ，开启 tinyint(1) 转 Boolean (主要解决 mysql boolean 类型到 pg boolean 的转换，目前主要是 quartz 的表字段需要转换)
```

#### 第4步 使用dwh 一键构建mysql兼容函数 功能，这样子就可以畅玩PG版若依了
```properties
# 添加 mysql 常用函数
# 添加 mysql 兼容函数方法(通过 dwh 添加常用mysql函数即可一步到位)
if
ifnull
group_concat
find_in_set
sysdate()
date_format
```


