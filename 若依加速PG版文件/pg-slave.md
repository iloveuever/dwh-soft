
### 若依版本地址(v4.7.8)：https://gitee.com/y_project/RuoYi
目前是将修改后的代码直接打包压缩上传，便于大家对照修改的信息项
### 视频地址: https://www.bilibili.com/video/BV1E2421M7yv/

### 方案1，使用PG作为若依的加速查询库
#### 第一步，新增PG数据源
在 application-druid.yml 文档下配置slave数据源信息(将PG相关的配置维护上即可) 和 分页工具pagehelper自动切换数据库配置

#### 第二步，使用dwh同步实时同步若依数据到PG库

#### 第三步，使用 @DataSource(DataSourceType.SLAVE) 在打算加速的sql语句场景上使用

#### 第四步，启动若依即可，会发现控制台打印了 切换到slave数据源




